var FoodArray = [
  "https://skinnyms.com/wp-content/uploads/2015/09/Savory-Lemon-White-Fish-Fillets.jpg",
  "https://assets.biggreenegg.eu/app/uploads/2019/03/28145521/topimage-classic-hamburger-2019m04-800x534.jpg",
  "https://i.ndtvimg.com/i/2018-02/fries_620x330_51517901541.jpg",
  "https://images-gmi-pmc.edge-generalmills.com/f4c0a86f-b080-45cd-a8a7-06b63cdb4671.jpg",
  "https://www.gimmesomeoven.com/wp-content/uploads/2012/11/hummus-mashed-potatoes-1.jpg",
  "https://cdn.cpnscdn.com/static.coupons.com/ext/kitchme/images/recipes/600x400/low-carb-southern-fried-chicken-recipe_16091.jpg",
  "https://www.cookingclassy.com/wp-content/uploads/2018/01/instant-pot-spaghetti-12-500x500.jpg",
  "https://www.africanbites.com/wp-content/uploads/2018/03/IMG_9302.jpg",
  "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fcdn-image.myrecipes.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2Fultimate-grilled-cheese-sl.jpg%3Fitok%3DH9Kt9wEq&w=450&c=sc&poi=face&q=85"
]


var FoodKeys = FoodArray.keys();

function openForm() {
    document.getElementById("myForm").style.display = "block";
  }
  
  function closeForm() {
    document.getElementById("myForm").style.display = "none";
  }


function testing()
{
  var length = FoodArray.length;

  for (var i= 0; i < length; i++)
  {
  document.getElementById("testGrid").innerHTML += '<img src="'+FoodArray[i]+'">';
  }
}

function testing_add()
{
  var getAdd = document.getElementById("add").value;
  FoodArray.push(getAdd);
  FoodKeys = FoodArray.keys();
  optionSelect();
  clearBox("selectTest");
  optionSelect();
  document.getElementById("testGrid").innerHTML += '<img src="'+getAdd+'">';

}

function testing_remove()
{

  FoodArray.pop();
  FoodKeys = FoodArray.keys();
  optionSelect();
  clearBox("testGrid");
  testing();
  clearBox("selectTest");
  optionSelect();
}

function clearBox(value)
{
    document.getElementById(value).innerHTML = "";
}


function optionSelect()
{
  FoodKeys = FoodArray.keys();
  for (x of FoodKeys) {
    document.getElementById("selectTest").innerHTML += "<option value="+x+">"+x+"</option>";
  }
}

function selectRemove()
{
  var e = document.getElementById("selectTest");
  var value = selectTest.options[selectTest.selectedIndex].value;
  FoodArray.splice(value,1);
  clearBox("testGrid");
  clearBox("selectTest");
  testing();
  optionSelect();
}


function testing_edit()
{
  var e = document.getElementById("editTest");
  var value = e.options[e.selectedIndex].value;
  var getEdit = document.getElementById("edit").value;

  FoodArray[value] = getEdit;
  clearBox("selectTest");
  optionSelect();
  clearBox("testGrid");
  testing();

}